﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogController : CharacterBase
{
    [SerializeField] protected Transform[] waypoints;
    [SerializeField] protected DropZone targetZone;
    [SerializeField] protected DogFeeder feeder;

    public void StartBehaviour()
    {
        SetActiveCharacter(true);
        TakeDecision();
        feeder.OnFeederEmpty += TakeDecision;
    }

    protected void TakeDecision()
    {
        if (!isActive)
            return;

        if (feeder.remainingFood > 0 && !isDragging)
            MoveTo(feeder.transform.position, StartEating);
        else if (targetZone != null && targetZone.zoneCompleted && !isDragging)
            GoToSnatch();
        else
            RandomWalk();
    }

    protected void RandomWalk()
    {
        
       Transform nextTarget = waypoints[Random.Range(0, waypoints.Length)];
        MoveTo(nextTarget.position, TakeDecision);

        Debug.Log("Sending to: " + nextTarget.position);

    }

    public void SendToSnatch(DropZone zoneToSnatch = null)
    {
        targetZone = zoneToSnatch;
    }

    private void GoToSnatch()
    {
        MoveTo(targetZone.DropPlace, Snatch);
    }

    private void Snatch()
    {
        if (feeder.remainingFood > 0 && !isDragging)
        {
            MoveTo(feeder.transform.position, StartEating);
            return;
        }

        Draggable snatchedItem = targetZone.SnatchDraggable();

        GrabObject(snatchedItem);

        if(snatchedItem != null)
            TakeDecision();
    }

    private void StartEating()
    {
        feeder.StartFeedingDog();
    }

    public void StopBehaviour()
    {
        StopAllCoroutines();
        isActive = false;
        navAgent.isStopped = true;
        feeder.OnFeederEmpty -= TakeDecision;
    }
    
}