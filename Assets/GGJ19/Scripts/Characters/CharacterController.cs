﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : CharacterBase
{
    [SerializeField] protected Image staminaSlider;
    [SerializeField] [Range(0, 3)] protected float recoveryRate = 1;
    [SerializeField] protected float timeToRecover = 3;
    protected float stamina;
    private string moveH = "P0H", moveV = "P0V", actionB = "P0A";

    public int SetJoystickID { set { moveH = "P" + value + "H"; moveV = "P" + value + "V"; actionB = "P" + value + "A"; } }

    protected bool isTired;
    protected float tiredTime;

    public int score { get; private set; }

    protected override void Awake()
    {
        base.Awake();
        stamina = 100;
        SetActiveCharacter(true);
    }

    protected override void Recover(int amount)
    {
        stamina += amount;

        if (stamina > 100)
            stamina = 100;
    }

    protected void UpdateStaminaBar()
    {
        if (staminaSlider == null)
            return;

        staminaSlider.fillAmount = stamina / 100f;

    }

    protected override void Move(float _x, float _y)
    {
        if (_x == 0 && _y == 0)
        {
            RecoverStamina();
            return;
        }

        base.Move(_x, _y);

        stamina -= Time.deltaTime * energyDecreaseMultiplier * energyDecreaseFactor * new Vector2(_x, _y).magnitude;

        UpdateStaminaBar();

        if (stamina <= 0)
            GetTired();
    }

    public void GetTired()
    {
        isTired = true;
        tiredTime = 0;
    }

    protected void RecoverStamina()
    {
        float deltaRecovery = Time.deltaTime * energyDecreaseMultiplier * recoveryRate *1.7f;

        if (isDragging)
            deltaRecovery *= 0.5f;

        stamina = stamina > 100 ? 100 : stamina + deltaRecovery;
        UpdateStaminaBar();
    }

    private void Update()
    {
        if (moveH != "P0H")
        {
            if (!isActive)
                return;

            if (isTired)
            {
                tiredTime += Time.deltaTime;
                RecoverStamina();

                if (tiredTime > timeToRecover)
                {
                    isTired = false;
                    UpdateStaminaBar();
                }
            }
            else
            {
                KeyboardMove();
                DetectInteractables();

                if (Input.GetButton(actionB))
                    StartCoroutine(TryToInteract());
            }
        }
    }

    private void KeyboardMove()
    {
        Vector2 moveAmount = new Vector2(0, 0);

        if(Mathf.Abs(Input.GetAxis(moveH)) > 0.5f)
        {
            if (Input.GetAxis(moveH) > 0.5f)
                moveAmount.x++;
            else
                moveAmount.x--;
        }

        if (Mathf.Abs(Input.GetAxis(moveV)) > 0.5f)
        {
            if (Input.GetAxis(moveV) > 0.5f)
                moveAmount.y++;
            else
                moveAmount.y--;
        }

        moveAmount.Normalize();

        Move(moveAmount.x, moveAmount.y);
    }
}