﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactersManager : MonoBehaviour
{
    private CharacterController[] characters;
    [SerializeField]
    private GameObject[] playerSkin, newPlayer = null;
    [SerializeField]
    private Cinemachine.CinemachineTargetGroup cinemachine;

    private void Awake()
    {
        characters = GetComponentsInChildren<CharacterController>();
    }

    private void Start()
    {
        newPlayer = new GameObject[4];
    }

    public void ActivatePlayers(Player_Data[] playerData)
    {
        for (int i = 0; i < playerData.Length; i++)
        {
            if (playerData[i].playerID > 0)
            {
                cinemachine.AddMember(characters[i].transform, 1, 2);
                characters[i].SetActiveCharacter(i < playerData.Length);
                characters[i].SetJoystickID = playerData[i].joystickID;
                if(newPlayer[i] == null)
                    newPlayer[i] = Instantiate(playerSkin[playerData[i].skin], Vector3.zero, Quaternion.identity, characters[i].transform);
                newPlayer[i].transform.localPosition = Vector3.zero;
                Debug.Log("Instantiated " + i);
            }
        }
    }

    public void StartArea(Transform[] startPoints)
    {
        for (int i = 0; i < characters.Length; i++)
            characters[i].SetNewPosition(startPoints[i]);
    }

    public void StunAllPlayers()
    {
        for (int i = 0; i < characters.Length; i++)
            if (characters[i].isActive)
                characters[i].GetTired();
    }

    public int[] GetScores()
    {
        List<int> scores = new List<int>();

        for (int i = 0; i < characters.Length; i++)
            if (characters[i].isActive)
                scores.Add(characters[i].score);

        return scores.ToArray();
    }
}
