﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class CharacterBase : MonoBehaviour
{
    [SerializeField] protected float interactRange = 0.5f;
    [SerializeField] protected float speedMultiplier = 1;
    [SerializeField] protected float energyDecreaseMultiplier = 1;

    [SerializeField] protected Transform dragPoint;

    [Space]
    [Header("Movement")]
    [SerializeField] protected float rotationSpeed = 5;

    protected float speedFactor = 1;
    protected float energyDecreaseFactor = 1;

    protected NavMeshAgent navAgent;

    protected Draggable currentDraggable;
    protected DragInfo currentDragInfo;

    protected InteractableBase currentInteractable;
    protected CharacterBase chartToInteract;

    public bool isDragging;
    public bool isActive;
    private bool isUsingAction = false;

    protected bool runButtonPress;

    protected virtual void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
        isDragging = false;
    }

    public void SetActiveCharacter(bool isTrue)
    {
        gameObject.SetActive(isTrue);
        isActive = isTrue;
    }

    protected void GrabObject(Draggable dragRefference = null)
    {
        if (isDragging)
            return;

        if(dragRefference == null)
        {
            if (currentInteractable == null)
                return;

            currentDraggable = currentInteractable.gameObject.GetComponent<Draggable>();
        }
        else
        {
            currentDraggable = dragRefference;
        }
       

        currentDragInfo = currentDraggable.GetDragInfo();

        speedFactor -= currentDragInfo.speedPenalty;
        energyDecreaseFactor += currentDragInfo.energyPenalty;

        isDragging = true;
        currentDraggable.SetDrag(dragPoint);
        
    }

    protected void UngrabObject()
    {
        isDragging = false;
        speedFactor = 1;
        energyDecreaseFactor = 1;

        if(currentDraggable != null)
        {
            currentDraggable.Undrag();
            currentDraggable = null;
        }

        if(navAgent.remainingDistance > 0.1f && navAgent.hasPath)
            navAgent.speed = speedMultiplier * speedFactor;
    }

    protected void MoveTo(Vector3 Objetive, UnityAction OnComplete)
    {
        navAgent.speed = speedMultiplier * speedFactor;
        navAgent.SetDestination(Objetive);
        navAgent.isStopped = true;
        StartCoroutine(WaitToReachDestination(OnComplete));
    }

    protected IEnumerator WaitToReachDestination(UnityAction OnComplete)
    {
        while (navAgent.pathPending)
            yield return null;
        
        navAgent.isStopped = false;

        while (navAgent.remainingDistance > 0.1f && navAgent.hasPath)
            yield return null;

        if (OnComplete != null)
            OnComplete();
    }

    protected virtual void Move(float _x, float _y)
    {
        Vector3 direction = new Vector3(_x, 0, _y);
        navAgent.Move(direction * speedFactor * speedMultiplier*Time.deltaTime);
        transform.rotation = Quaternion.LookRotation(Vector3.Lerp(transform.forward, direction, Time.deltaTime*rotationSpeed), Vector3.up);
    }

    protected void DetectInteractables()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, interactRange))
        {
            InteractableBase newInteractable = hit.collider.GetComponent<InteractableBase>();

            if (newInteractable != null)
            {
                if (newInteractable != currentInteractable)
                {
                    if (currentInteractable != null)
                        currentInteractable.SetInteractCondition(false);

                    currentInteractable = newInteractable;
                    currentInteractable.SetInteractCondition(true);
                }

                if (chartToInteract != null)
                    chartToInteract = null;

            }
            else
            {
                CharacterBase newChar = hit.collider.GetComponent<CharacterBase>();

                if (newChar != chartToInteract)
                {
                    chartToInteract = newChar;
                    Debug.Log("Character Found");
                }

                if (currentInteractable != null)
                {
                    currentInteractable.SetInteractCondition(false);
                    currentInteractable = null;
                }
            }
            
        }
        else
        {
            if (currentInteractable != null)
            {
                currentInteractable.SetInteractCondition(false);
                currentInteractable = null;
            }

            if (chartToInteract != null)
                chartToInteract = null;
        }
    }

    protected IEnumerator TryToInteract()
    {
        if (!isUsingAction)
        {
            isUsingAction = true;
            if (currentInteractable != null)
            {
                UseInteractable();

                switch (currentInteractable.interactType)
                {
                    case InteractableType.Draggable:
                        if (!isDragging)
                            GrabObject();
                        break;
                    case InteractableType.Recovery:
                        Recover(currentInteractable.recoveryFactor);
                        break;

                    case InteractableType.Usable:
                        break;
                }
            }
            else if (isDragging)
            {
                UngrabObject();
            }
            else if (chartToInteract != null)
            {
                Draggable newDraggable = chartToInteract.GetItemSnatched();

                Debug.Log("Snatching item " + newDraggable != null);

                if (newDraggable != null)
                    GrabObject(newDraggable);
            }
            yield return new WaitForSeconds(0.5f);
            isUsingAction = false;
        }
    }

    protected virtual void UseInteractable()
    {
        currentInteractable.UseInteractable();
    }

    protected virtual void Recover(int amount)
    {

    }

    public Draggable GetItemSnatched()
    {
        if (!isDragging)
            return null;

        Draggable draggableToSnatch = currentDraggable;

        UngrabObject();

        return draggableToSnatch;
    }

    public void SetNewPosition(Transform newReference)
    {
        transform.position = newReference.position;
        transform.rotation = newReference.rotation;
    }
}
