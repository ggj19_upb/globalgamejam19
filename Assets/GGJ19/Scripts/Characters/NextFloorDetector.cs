﻿using UnityEngine;
using UnityEngine.Events;

public class NextFloorDetector : MonoBehaviour
{
    private event UnityAction OnPlayerReachTrigger;

    private Collider trigger;

    private void Awake()
    {
        trigger = GetComponent<Collider>();
        trigger.isTrigger = true;
        trigger.enabled = false;
    }

    public void ActivateDetector(UnityAction OnComplete)
    {
        OnPlayerReachTrigger = OnComplete;
        trigger.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        CharacterController character = other.GetComponent<CharacterController>();

        if(character != null)
        {
            trigger.enabled = false;

            if (OnPlayerReachTrigger != null)
                OnPlayerReachTrigger();

            OnPlayerReachTrigger = null;
        }
    }
}
