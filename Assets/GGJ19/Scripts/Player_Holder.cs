﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Holder : MonoBehaviour
{
    [SerializeField]
    private Player_Data[] player;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
