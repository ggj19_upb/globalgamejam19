﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharMaker : MonoBehaviour
{
    public GameObject[] playerPrefab;

    public PlayerController[] CreateNewChars(Player_Data[] pData)
    {
        Debug.Log("Start Making char " + pData.Length);
        PlayerController[] player = new PlayerController[pData.Length];
        for (int j = 0; j < pData.Length - 1; j++)
        {
            if (pData[j].joystickID > 0)
            {
                Debug.Log("Making char");
                GameObject newGO = Instantiate(playerPrefab[pData[j].skin], new Vector3(UnityEngine.Random.Range(-2.5f, 2.5f), 0, UnityEngine.Random.Range(-2.5f, 2.5f)), Quaternion.identity, null);
                newGO.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                player[j] = newGO.GetComponent<PlayerController>();
            }
        }
        return player;
    }
}
