﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DogFeeder : InteractableBase
{
    public event UnityAction OnFeederEmpty;

    [SerializeField] private Renderer foodRenderer;
    [SerializeField] private float timeToEmpty = 10;

    public float remainingFood;
    
    protected override void Awake()
    {
        base.Awake();
        remainingFood = 0;
        interactType = InteractableType.Usable;
    }

    public override void UseInteractable()
    {
        base.UseInteractable();

        remainingFood = timeToEmpty;
        foodRenderer.enabled = true;
    }

    public void StartFeedingDog()
    {
        StartCoroutine(FeedingProcess());
    }

    private IEnumerator FeedingProcess()
    {
        while(remainingFood > 0)
        {
            remainingFood -= Time.deltaTime;

            yield return null;
        }

        foodRenderer.enabled = false;

        if (OnFeederEmpty != null)
            OnFeederEmpty();
    }

}
