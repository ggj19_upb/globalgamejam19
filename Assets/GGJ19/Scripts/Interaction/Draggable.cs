﻿using UnityEngine;

public class Draggable : InteractableBase
{
    [SerializeField] protected Transform dragPoint;
    [SerializeField] protected DraggableType dragType;
    [SerializeField] [Range(0, 1)] protected float energyPenalty = 0;
    [SerializeField] [Range(0, 1)] protected float speedPenalty = 0;

    protected Vector3 dragCorrection;
    protected Transform originalParent;

    protected Rigidbody rb;

    protected override void Awake()
    {
        base.Awake();

        interactType = InteractableType.Draggable;
        dragCorrection = transform.position - dragPoint.position;
        originalParent = transform.parent;

        rb = GetComponent<Rigidbody>();
    }

    public void SetDrag(Transform newParent)
    { 
        transform.parent = newParent;
        transform.localPosition = dragCorrection;
        transform.localRotation = Quaternion.identity;
        SetPhysicsActive(false);
    }

    public DragInfo GetDragInfo()
    {
        return new DragInfo(dragType, energyPenalty, speedPenalty);
    }

    public void Undrag()
    {
        transform.parent = originalParent;
        SetPhysicsActive(true);
    }

    private void  SetPhysicsActive(bool isTrue)
    {
        col.enabled = isTrue;

        if (rb == null)
            return;

        rb.isKinematic = !isTrue;
        rb.useGravity = isTrue;
    }
}

public class DragInfo
{
    public DraggableType type;
    public float energyPenalty;
    public float speedPenalty;

    public DragInfo(DraggableType _type, float _energyPenalty, float _speedPenalty)
    {
        type = _type;
        energyPenalty = _energyPenalty;
        speedPenalty = _speedPenalty;
    }
}

public enum DraggableType
{
    None,
    Book,
    Beer,
    Bag,
    Pizza,
    Mekato,
    bolsaBasura,
    Condon,
    Globo,
    vasos,
    Trapeadora
}
