﻿using UnityEngine;

public class LightSwitch : InteractableBase
{
    [SerializeField] private bool startOn;

    private Light switchLight;

    protected override void Awake()
    {
        base.Awake();
        switchLight = GetComponentInChildren<Light>();
        switchLight.enabled = startOn;
        interactType = InteractableType.Usable;
    }

    public override void UseInteractable()
    {
        base.UseInteractable();

        switchLight.enabled = !switchLight.enabled;
    }
}
