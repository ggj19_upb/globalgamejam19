﻿using UnityEngine;

public class DualLightSwitch : InteractableBase
{
    private Light[] switchLights;
    private Collider[] switchColliders;

    protected override void Awake()
    {
        base.Awake();
        switchLights = GetComponentsInChildren<Light>();
        switchColliders = GetComponents<Collider>();
        interactType = InteractableType.Usable;
    }

    public override void UseInteractable()
    {
        base.UseInteractable();

        for (int i = 0; i < switchLights.Length; i++)
            switchLights[i].enabled = !switchLights[i].enabled;

        for (int i = 0; i < switchColliders.Length; i++)
            switchColliders[i].enabled = !switchColliders[i].enabled;
    }
}
