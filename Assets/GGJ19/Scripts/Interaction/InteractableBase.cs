﻿using UnityEngine;

public class InteractableBase : MonoBehaviour
{
    public InteractableType interactType;
    public int recoveryFactor = 0;

    protected Collider col;

    protected virtual void Awake()
    {
        col = GetComponent<Collider>();
    }

    public void SetInteractCondition(bool isOnRange)
    {
        if(isOnRange)
        {

        }
        else
        {

        }
    }

    public virtual void UseInteractable()
    {

    }

}

public enum InteractableType
{
    Draggable,
    Usable,
    Recovery
}
