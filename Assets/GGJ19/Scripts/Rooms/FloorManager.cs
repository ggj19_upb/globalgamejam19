﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FloorManager : MonoBehaviour
{
    public event UnityAction OnFloorCompleted;

    private DropZone[] dropZones;
    private GarbageCollector collector;
    private Trapeable[] aguas;
    // hay que poner los charcos

    private void Awake()
    {
        dropZones = GetComponentsInChildren<DropZone>();
        collector = GetComponentInChildren<GarbageCollector>();
        aguas = GetComponentsInChildren<Trapeable>();
    }

    public void InitializeFloor()
    {
        for (int i = 0; i < dropZones.Length; i++)
            dropZones[i].OnItemPlaced += VerifyFloorCompleted;

        for (int i = 0; i < aguas.Length; i++)
            aguas[i].OnCleaned += VerifyFloorCompleted;

        collector.OnAllItemPlaced += VerifyFloorCompleted;
    }

    private void VerifyFloorCompleted()
    {
        if (!collector.zoneCompleted)
            return;

        for (int i = 0; i < dropZones.Length; i++)
            if (!dropZones[i].zoneCompleted)
                return;

        for (int i = 0; i < aguas.Length; i++)
            if (!aguas[i].isCompleted)
                return;
        
        FloorCompleted();
    }

    private void FloorCompleted()
    {
        for (int i = 0; i < dropZones.Length; i++)
            dropZones[i].OnItemPlaced -= VerifyFloorCompleted;

        collector.OnAllItemPlaced -= VerifyFloorCompleted;

        if (OnFloorCompleted != null)
            OnFloorCompleted();
    }


    public DropZone GetRandomDropZone()
    {
        List<DropZone> completedZones = new List<DropZone>();

        for (int i = 0; i < dropZones.Length; i++)
            if (dropZones[i].zoneCompleted)
                completedZones.Add(dropZones[i]);

        if (completedZones.Count == 0)
            return null;

        return completedZones[Random.Range(0, completedZones.Count)];
    }
}
