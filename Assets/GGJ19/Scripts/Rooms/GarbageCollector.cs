﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GarbageCollector : MonoBehaviour
{
    public UnityAction OnAllItemPlaced;

    public bool zoneCompleted;

    [SerializeField] private List<DraggableType> wantedItems;
    [SerializeField] private Transform dropPoint;

    public Vector3 DropPlace { get { return dropPoint.position; } }

    private Collider col;

    private void Awake()
    {
        col = GetComponent<Collider>();
        col.isTrigger = true;
    }

    private void PutItemInZone(Draggable draggable)
    {
        for(int i=0; i<wantedItems.Count; i++)
            if (draggable.GetDragInfo().type == wantedItems[i])
            {
                draggable.gameObject.SetActive(false);
                wantedItems.Remove(draggable.GetDragInfo().type);
                break;
            }

        if(wantedItems.Count == 0)
        {
            col.enabled = false;
            zoneCompleted = true;

            if (OnAllItemPlaced != null)
                OnAllItemPlaced();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Something entered");

        Draggable enteringDraggable = other.GetComponent<Draggable>();

        if (enteringDraggable != null)
            PutItemInZone(enteringDraggable);
    }
}
