﻿using UnityEngine;
using UnityEngine.Events;

public class DropZone : MonoBehaviour
{
    public UnityAction OnItemPlaced;

    public bool zoneCompleted;

    [SerializeField] private DraggableType wantedItem;
    [SerializeField] private Transform dropPoint;

    public Vector3 DropPlace { get { return dropPoint.position;  } }

    private Collider col;
    private Draggable storedDraggable;

    private void Awake()
    {
        col = GetComponent<Collider>();
        col.isTrigger = true;
    }

    private void PutItemInZone(Draggable draggable)
    {
        if(draggable.GetDragInfo().type == wantedItem)
        {
            draggable.SetDrag(dropPoint);
            storedDraggable = draggable;
            col.enabled = false;
            zoneCompleted = true;

            if (OnItemPlaced != null)
                OnItemPlaced();
        }
        
    }

    public Draggable SnatchDraggable()
    {
        Draggable current = storedDraggable;
        storedDraggable = null;
        col.enabled = true;
        zoneCompleted = false;
        return current;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Something entered");

        Draggable enteringDraggable = other.GetComponent<Draggable>();

        if (enteringDraggable != null)
            PutItemInZone(enteringDraggable);
    }
}
