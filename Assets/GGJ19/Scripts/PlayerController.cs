﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 100;

    public void Move(Vector3 direction)
    {
        transform.Translate(direction.normalized * speed * Time.deltaTime);
    }

    public void Action()
    {
        Debug.Log("Action!!");
    }
}
