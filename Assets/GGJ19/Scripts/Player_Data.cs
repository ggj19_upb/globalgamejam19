﻿using System;
using UnityEngine;

[CreateAssetMenu] [Serializable]
public class Player_Data : ScriptableObject
{
    public int playerID = -1;
    public int skin = 0;
    public int joystickID = 0;

    //public int SetPlayerID { set { playerID = value; } get { return playerID; } }
    //public string SetSkin { set { skin = value; } get { return skin; } }
    //public int SetJoystickID { set { joystickID = value; } get { return joystickID; } }
}