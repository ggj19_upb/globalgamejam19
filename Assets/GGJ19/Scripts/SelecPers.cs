﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelecPers : MonoBehaviour
{
    [SerializeField]
    private CharMaker maker;
    public PlayerController[] controllers;
    public int[] joyPlay;
    public GameObject[] selP1, selP2, selP3, selP4, pSkin;
    public Player_Data[] pData;

    private void Start()
    {
        joyPlay = new int[4];
        Debug.Log("Data " + pData.Length);
        for (int p = 0; p < pData.Length - 1; p++)
        {
            joyPlay[p] = pData[p].joystickID;
        }
        controllers = maker.CreateNewChars(pData);
    }

    private void Update()
    {
        if (Input.GetButton("GP"))
        {
            Debug.Log("A pressed");
        }

        for (int j = 1; j < 4; j++)
        {
            if (Input.GetButton("P" + j + "A"))
                Debug.Log("Joystick " + j + "pressed");
        }

        for (int p = 1; p <= controllers.Length; p++)
        {
            Debug.Log("Check player " + p);
            if (Input.GetButton("P" + p + "A"))
            {
                for (int j = 0; j < joyPlay.Length; j++)
                {
                    if (joyPlay[j] == p)
                        controllers[j].Action();
                }
            }
            if (Mathf.Abs(Input.GetAxis("P" + p + "H")) > 0.5f || Mathf.Abs(Input.GetAxis("P" + p + "V")) > 0.5f)
            {
                for(int j = 0; j < joyPlay.Length; j++)
                {
                    if (joyPlay[j] == p)
                        controllers[j].Move(new Vector3(Input.GetAxis("P" + p + "H"), 0, Input.GetAxis("P" + p + "V")));
                }
            }
        }
        Debug.Log("Joysticks " + Input.GetJoystickNames().Length);
    }
}
