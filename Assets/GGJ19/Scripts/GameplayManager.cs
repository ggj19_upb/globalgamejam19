﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    [SerializeField] private CharactersManager characters;
    [SerializeField] private Floor[] floors;
    [SerializeField] private Light levelLight;
    [SerializeField] private Player_Data[] playerData;
    [SerializeField] private AudioSpectrum audioSpectrum;

    private Floor currentFloor;
    private int nextFloorIndex;

    private bool gameWon, settingPlayer = false;

    float gameplayTime = 1000f;
    private bool isGameplayActive;

    private void Start()
    {
        gameWon = false;
        nextFloorIndex = 0;
        isGameplayActive = false;
        StartGameplay();
    }

    private void StartGameplay()
    {
        characters.ActivatePlayers(playerData);
        isGameplayActive = true;
        audioSpectrum.OnTimeCompleted += EndGameplay;
        audioSpectrum.OnJoderJugador += JoderALosJugadores;

        ChangeFloor();
    }

    private void ChangeFloor()
    {
        if(nextFloorIndex < floors.Length)
        {
            currentFloor = floors[nextFloorIndex];
            nextFloorIndex++;
            StartFloor();
        }
    }

    private void StartFloor()
    {
        characters.StartArea(currentFloor.playersStartPoints);

        if (currentFloor.dog != null)
            currentFloor.dog.StartBehaviour();

        currentFloor.manager.OnFloorCompleted += FloorCompleted;

        //// Aquí hay que suscrivirse a los eventos del audiomanager
    }

    private void FloorCompleted()
    {
        currentFloor.manager.OnFloorCompleted -= FloorCompleted;

        if (currentFloor.dog != null)
            currentFloor.dog.StopBehaviour();

        if(nextFloorIndex < floors.Length)
        {
            currentFloor.detector.ActivateDetector(ChangeFloor);
        }
        else
        {
            gameWon = true;
            EndGameplay();
        }
    }
    
    private void EndGameplay()
    {
        audioSpectrum.OnTimeCompleted -= EndGameplay;
        isGameplayActive = false;
        characters.GetScores(); /// hay que almacenar esto en algún lugar
        /// cargar la escena de acuerdo a si gano el juego
    }


    /// ///////////////////////////////////////////////////////////////////////////////

    private void JoderALosJugadores()
    {
        if (currentFloor.dog != null)
            SendDogToSnatch();
        else
            StunPlayers();
    }

    private void SendDogToSnatch()
    {
        if (currentFloor.dog != null)
            currentFloor.dog.SendToSnatch(currentFloor.manager.GetRandomDropZone());
        
    }


    private void StunPlayers()
    {
        characters.StunAllPlayers();

        if (levelLight != null)
            StartCoroutine(FlashPlayers());

    }

    private IEnumerator FlashPlayers()
    {
        while (levelLight.intensity < 10)
        {
            levelLight.intensity += Time.deltaTime * 20;
            yield return null;
        }

        while (levelLight.intensity > 1)
        {
            levelLight.intensity -= Time.deltaTime * 5;
            yield return null;
        }

        levelLight.intensity = 1;
    }



    private void Update()
    {
        if (!isGameplayActive)
            return;

        gameplayTime -= Time.deltaTime;
        UpdateLevelTimer(gameplayTime);

        if (gameplayTime <= 0)
            EndGameplay();

        for (int j = 1; j <= 4; j++)
        {
            if (Input.GetButton("P" + j + "A"))
            {
                if (!settingPlayer)
                {
                    settingPlayer = true;
                    bool alreadyActive = false;
                    for (int p = 0; p < playerData.Length; p++)
                    {
                        if (playerData[p].joystickID == j)
                        {
                            alreadyActive = true;
                        }
                    }
                    if (!alreadyActive)
                    {
                        Debug.Log("Set new player");
                        for(int newPlayer = 0; newPlayer < playerData.Length; newPlayer++)
                        {
                            Debug.Log("Cheching spot " + newPlayer);
                            if (playerData[newPlayer].playerID < 1)
                            {
                                Debug.Log("Spot found " + newPlayer + " id " + (newPlayer + 1));
                                playerData[newPlayer].playerID = newPlayer + 1;
                                playerData[newPlayer].skin = UnityEngine.Random.Range(0,3);
                                playerData[newPlayer].joystickID = j;
                                newPlayer = playerData.Length;
                            }
                        }
                        characters.ActivatePlayers(playerData);
                    }
                }
            }
        }
    }

    public void UpdateLevelTimer(float totalSeconds)
    {
        int minutes = Mathf.FloorToInt(totalSeconds / 60f);
        int seconds = Mathf.RoundToInt(totalSeconds % 60f);

        string formatedSeconds = seconds.ToString();

        if (seconds == 60)
        {
            seconds = 0;
            minutes += 1;
        }

        //timer.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }


}

[System.Serializable]
public class Floor
{
    public FloorManager manager;
    public DogController dog;
    public Transform[] playersStartPoints;
    public NextFloorDetector detector;

}
