﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.SceneManagement;

public class PlayerSelect : MonoBehaviour
{
    public Player_Data[] players;
    private int currentPlayers = 0;
    public GameObject[] playerUI, character, skins, playerRdy;
    private bool settingPlayer = false, startingGame = false;
    public Transform[] playerSkinPos;
    public bool[] pSkin;
    public Coroutine[] cSkin;

    private void Start()
    {
        pSkin = new bool[4];
        cSkin = new Coroutine[4];
        currentPlayers = 0;
        startingGame = false;
        for (int i = 0; i < players.Length; i++)
        {
            playerUI[2 * i].SetActive(true);
            playerUI[2 * i + 1].SetActive(false);
            character[i].SetActive(false);
            players[i].playerID = -1;
            players[i].joystickID = 0;
            players[i].skin = i;
            pSkin[i] = false;
            playerRdy[i].SetActive(false);
            if(cSkin[i] != null)
                StopCoroutine(cSkin[i]);
        }
    }

    void Update()
    {
        Debug.Log("Current: " + currentPlayers);
        if (currentPlayers < 5)
        {
            for (int j = 1; j <= 4; j++)
            {
                if (Input.GetButton("P" + j + "A"))
                {
                    Debug.Log("Joystick " + j + "pressed");
                    if (!settingPlayer)
                    {
                        settingPlayer = true;
                        StartCoroutine(SetNewPlayer(j));
                    }
                }
                if (Mathf.Abs(Input.GetAxis("P" + j + "H")) > 0.5f)
                {
                    for (int i = 0; i < players.Length; i++)
                    {
                        if (players[i].joystickID == j && !pSkin[i] && !playerRdy[i].activeSelf)
                        {
                            pSkin[i] = true;
                            if (cSkin[i] != null)
                                StopCoroutine(cSkin[i]);
                            switch (i)
                            {
                                case 0:
                                    cSkin[i] = StartCoroutine(ChangeSkin1(Input.GetAxis("P1H") < 0 ? false : true, 0));
                                    break;
                                case 1:
                                    cSkin[i] = StartCoroutine(ChangeSkin2(Input.GetAxis("P2H") < 0 ? false : true, 1));
                                    break;
                                case 2:
                                    cSkin[i] = StartCoroutine(ChangeSkin3(Input.GetAxis("P3H") < 0 ? false : true, 2));
                                    break;
                                case 3:
                                    cSkin[i] = StartCoroutine(ChangeSkin4(Input.GetAxis("P4H") < 0 ? false : true, 3));
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    private IEnumerator ChangeSkin4(bool right, int player)
    {
        if (right)
        {
            Debug.Log("Right!");
            players[player].skin = players[player].skin + 1 > 3 ? 0 : players[player].skin + 1;
            Destroy(playerSkinPos[player].GetChild(0).gameObject);
            GameObject skin = Instantiate(skins[players[player].skin], Vector3.zero, Quaternion.Euler(0, 180, 0), playerSkinPos[player]);
            skin.transform.localPosition = Vector3.zero;
        }
        else
        {
            Debug.Log("Left!");
            players[player].skin = players[player].skin - 1 < 0 ? 3 : players[player].skin - 1;
            Destroy(playerSkinPos[player].GetChild(0).gameObject);
            GameObject skin = Instantiate(skins[players[player].skin], Vector3.zero, Quaternion.Euler(0, 180, 0), playerSkinPos[player]);
            skin.transform.localPosition = Vector3.zero;
        }
        yield return new WaitForSeconds(0.3f);
        pSkin[3] = false;
    }

    private IEnumerator ChangeSkin3(bool right, int player)
    {
        if (right)
        {
            Debug.Log("Right!");
            players[player].skin = players[player].skin + 1 > 3 ? 0 : players[player].skin + 1;
            Destroy(playerSkinPos[player].GetChild(0).gameObject);
            GameObject skin = Instantiate(skins[players[player].skin], Vector3.zero, Quaternion.Euler(0, 180, 0), playerSkinPos[player]);
            skin.transform.localPosition = Vector3.zero;
        }
        else
        {
            Debug.Log("Left!");
            players[player].skin = players[player].skin - 1 < 0 ? 3 : players[player].skin - 1;
            Destroy(playerSkinPos[player].GetChild(0).gameObject);
            GameObject skin = Instantiate(skins[players[player].skin], Vector3.zero, Quaternion.Euler(0, 180, 0), playerSkinPos[player]);
            skin.transform.localPosition = Vector3.zero;
        }
        yield return new WaitForSeconds(0.3f);
        pSkin[2] = false;
    }

    private IEnumerator ChangeSkin2(bool right, int player)
    {
        if (right)
        {
            Debug.Log("Right!");
            players[player].skin = players[player].skin + 1 > 3 ? 0 : players[player].skin + 1;
            Destroy(playerSkinPos[player].GetChild(0).gameObject);
            GameObject skin = Instantiate(skins[players[player].skin], Vector3.zero, Quaternion.Euler(0, 180, 0), playerSkinPos[player]);
            skin.transform.localPosition = Vector3.zero;
        }
        else
        {
            Debug.Log("Left!");
            players[player].skin = players[player].skin - 1 < 0 ? 3 : players[player].skin - 1;
            Destroy(playerSkinPos[player].GetChild(0).gameObject);
            GameObject skin = Instantiate(skins[players[player].skin], Vector3.zero, Quaternion.Euler(0, 180, 0), playerSkinPos[player]);
            skin.transform.localPosition = Vector3.zero;
        }
        yield return new WaitForSeconds(0.3f);
        pSkin[1] = false;
    }

    private IEnumerator ChangeSkin1(bool right, int player)
    {
        if (right)
        {
            Debug.Log("Right!");
            players[player].skin = players[player].skin + 1 > 3 ? 0 : players[player].skin + 1;
            Destroy(playerSkinPos[player].GetChild(0).gameObject);
            GameObject skin = Instantiate(skins[players[player].skin], Vector3.zero, Quaternion.Euler(0, 180, 0), playerSkinPos[player]);
            skin.transform.localPosition = Vector3.zero;
        }
        else
        {
            Debug.Log("Left!");
            players[player].skin = players[player].skin - 1 < 0 ? 3 : players[player].skin - 1;
            Destroy(playerSkinPos[player].GetChild(0).gameObject);
            GameObject skin = Instantiate(skins[players[player].skin], Vector3.zero, Quaternion.Euler(0, 180, 0), playerSkinPos[player]);
            skin.transform.localPosition = Vector3.zero;
        }
        yield return new WaitForSeconds(0.3f);
        pSkin[0] = false;
    }

    private IEnumerator SetNewPlayer(int j)
    {
        bool alreadyActive = false;
        for (int p = 0; p < players.Length; p++)
        {
            if (players[p].joystickID == j)
            {
                alreadyActive = true;
                playerRdy[p].SetActive(true);
                bool allReady = true;
                for(int r = 0; r < players.Length; r++)
                {
                    if(players[r].playerID > 0 && !playerRdy[r].activeSelf)
                        allReady = false;
                }
                if(allReady)
                {
                    startingGame = true;
                    Debug.Log("Ready!!!");
                    yield return new WaitForSeconds(3.0f);
                    Initiate.Fade("Cinematica", Color.black, 1); //MechanicsTesting Cinematica
                    //SceneManager.LoadScene("Cinematica");
                    Debug.Log("Start!!!");
                    yield return new WaitForSeconds(5);
                }
            }
        }
        Debug.Log("Active? " + alreadyActive);
        if (!alreadyActive && !startingGame)
        {
            for (int np = 0; np < players.Length; np++)
            {
                Debug.Log("Searhing place " + np + " in " + players.Length + " rule " + players[np].playerID);
                if (players[np].playerID < 1)
                {
                    currentPlayers++;
                    players[np].playerID = currentPlayers;
                    players[np].joystickID = j;
                    playerUI[2 * np].SetActive(false);
                    playerUI[2 * np + 1].SetActive(true);
                    GameObject skin = Instantiate(skins[np], Vector3.zero, Quaternion.Euler(0 , 180, 0), playerSkinPos[np]);
                    skin.transform.localPosition = Vector3.zero;
                    character[np].SetActive(true);
                    //np = players.Length;
                    break;
                }
            }
        }
        yield return new WaitForSeconds(0.5f);
        settingPlayer = false;
    }
}
