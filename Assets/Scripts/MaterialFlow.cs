﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaterialFlow : MonoBehaviour
{
    public float velocidad = 0.5f;
    [Space]
    public float valorMax = 15;
    public float valorMin = 5;

    private Image img;
    private float scl;
    private bool dir;

    private void Awake()
    {
        img = GetComponent<Image>();
        scl = valorMin;
    }

    private void Update()
    {
        if (dir)
        {
            scl += velocidad;
        }
        else
        {
            scl -= velocidad;
        }

        if (scl >= valorMax)
        {
            dir = false;
        }
        if (scl <= valorMin)
        {
            dir = true;
        }

        img.color = new Color32(255, 255, 255, (byte)scl);
    }
}
