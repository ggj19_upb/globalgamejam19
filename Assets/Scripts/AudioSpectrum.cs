﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent (typeof (AudioSource))]
public class AudioSpectrum : MonoBehaviour
{
    public event UnityAction OnTimeCompleted;
    public event UnityAction OnJoderJugador;

    AudioSource source;
    public static float[] sample = new float[512];
    public static float[] freqBand = new float[8];
    public static float[] bandBuffer = new float[8];
    private float[] bufferDecrease = new float[8];

    public static float amplitude, amplitudeBuffer;
    public float tiempoBajada;
    public float amplitudeHighest;

    [Space]
    public Text timer;
    private int minutes = 5;
    private float seconds = 0;
    private bool perdio;
    private bool sent;
    private float delayPerMessage = 15;
    private bool esperaMessage;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
        //Initiate.Fade()
    }

    private void Update()
    {
        if (minutes == -1)
        {
            perdio = true;
        }

        if (!perdio)
        {
            if (seconds <= 0)
            {
                minutes -= 1;
                seconds = 60;
            }

            seconds -= Time.deltaTime;

            if (minutes == 0)
            {
                timer.color = new Color32(55, 0, 0, 255);
            }

            timer.text = minutes + ":" + Mathf.CeilToInt(seconds).ToString("00");
        }
        else if(!sent)
        {
            sent = true;

            timer.text = "END";

            if (OnTimeCompleted != null)
                OnTimeCompleted();
        }

        if (amplitude > 0.8f && esperaMessage == false)
        {
            if (OnJoderJugador != null)
                OnJoderJugador();

            esperaMessage = true;
            Debug.Log("joder");

            Invoke("EsperaMensaje", 15);
        }
    }

    private void EsperaMensaje()
    {
        esperaMessage = false;
    }

    void FixedUpdate()
    {
        GetSpectrum();
        MakeFrequencyBands();
        BandBuffer();
        GetAmplitude();

        //Debug.Log(amplitude);

        if (amplitudeHighest > 0)
        {
            amplitudeHighest = amplitudeHighest - tiempoBajada;
        }
	}

    void GetSpectrum()
    {
        source.GetSpectrumData(sample, 0, FFTWindow.Blackman);
    }

    void BandBuffer()
    {
        for (int i = 0; i < 8; i++)
        {
            if (freqBand[i] > bandBuffer[i])
            {
                bandBuffer[i] = freqBand[i];
                bufferDecrease[i] = 0.005f;
            }
            if (freqBand[i] < bandBuffer[i])
            {
                bufferDecrease[i] = (bandBuffer[i] - freqBand[i]) / 8;
                bandBuffer[i] -= bufferDecrease[i];
            }
        }
    }

    void GetAmplitude()
    {
        float currentAmplitude = 0;
        float currentAmplitudeBuffer = 0;

        for (int i = 0; i < 8; i++)
        {
            currentAmplitude += freqBand[i];
            currentAmplitudeBuffer += bandBuffer[i];
        }

        if (currentAmplitude > amplitudeHighest)
        {
            amplitudeHighest = currentAmplitude;
        }

        amplitude = currentAmplitude / amplitudeHighest;
        amplitudeBuffer = currentAmplitudeBuffer / amplitudeHighest;
    }

    void MakeFrequencyBands()
    {
        int count = 0;

        for (int i = 0; i < 8; i++)
        {
            float average = 0;
            int sampleCount = (int)Mathf.Pow(2, i) * 2;

            if (i == 7)
            {
                sampleCount += 2;
            }

            for (int j = 0; j < sampleCount; j++)
            {
                average += sample[count] * (count + 1);
                count++;
            }

            average /= count;

            freqBand[i] = average * 10;
        }
    }
}
