﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicAudioStart : MonoBehaviour
{
    public float delay;

    private AudioSource cancion;

    private void Awake()
    {
        cancion = GetComponent<AudioSource>();
        StartCoroutine(IniciarDelay());
    }

    IEnumerator IniciarDelay()
    {
        yield return new WaitForSecondsRealtime(delay);
        cancion.Play();
    }
}
