﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorAcciones : MonoBehaviour
{
    public float Velocidad = 5;

    private Rigidbody rb;
    private Vector3 inputs;

    private readonly Vector3 zero = new Vector3(0, 0, 0);

    private void Awake()
    {
        inputs = zero;
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        inputs = zero;
        inputs.x = Input.GetAxis("Horizontal");
        inputs.z = Input.GetAxis("Vertical");

        if (inputs != zero)
        {
            transform.forward = inputs;
        }
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + inputs * Velocidad * Time.fixedUnscaledDeltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("interactuable"))
        {
            other.GetComponent<Outline>().enabled = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("interactuable"))
        {
            other.GetComponent<Outline>().enabled = false;
        }
    }
}