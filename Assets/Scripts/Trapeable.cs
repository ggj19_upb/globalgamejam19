﻿using UnityEngine;
using UnityEngine.Events;

public class Trapeable : MonoBehaviour
{
    public event UnityAction OnCleaned;

    private bool trapeado;
    private float escala = 1;

    public bool isCompleted { get; private set; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Trapeadora"))
        {
            trapeado = true;
        }
    }

    private void Update()
    {
        if (transform.localScale.x > 0 && trapeado)
        {
            escala -= 0.035f;
            transform.localScale = new Vector3(escala, escala, escala);
        }

        if (transform.localScale.x <= 0 && trapeado)
        {
            isCompleted = true;

            if (OnCleaned != null)
                OnCleaned();

            gameObject.SetActive(false);
        }
    }
}
