﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine;

[ExecuteInEditMode]
public class DefControl : MonoBehaviour
{
    public float FocusDistance;
    public PostProcessVolume post;

    DepthOfField def;

    private void Awake()
    {
        post.profile.TryGetSettings(out def);
    }

    private void Update()
    {
        def.focusDistance.value = FocusDistance;
    }
}
